<?php
namespace App\MVC\Controllers;
use Framework\Controller;

class HelloController extends Controller
{
    public function index(){
        return $this->view('index.php',['name'=>'Валиев']);
    }
    public function data(){
        return $this->view('data.php',['name'=>'Валиев','group'=>'605-71']);
    }
}